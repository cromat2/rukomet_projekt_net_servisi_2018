﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Entities;
using Rukomet.Repository;
using Rukomet.Repository.Interfaces;
using Rukomet.Repository.Repositories;

namespace Rukomet.API.Controllers.v1
{
    [Produces("application/json")]
    [ApiExplorerSettings(GroupName = "1.0")]
    [Route("api/{version:apiVersion}/Arena")]
    [ApiVersion("1.0")]
    [Authorize]
    public class ArenaController : Controller
    {
        private readonly IArenaRepository _repository;

        public ArenaController(IArenaRepository repository)
        {
            _repository = repository;
        }

        // GET: api/1.0/Arena
        [HttpGet]
        public IEnumerable<Arena> GetArenas()
        {
            return _repository.GetAll();
        }

        // GET: api/1.0/Arena/5
        [HttpGet("{id}")]
        public IActionResult GetArena([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                // Return 400 code
                return BadRequest(ModelState);
            }

            var Arena = _repository.Find(id);

            if (Arena == null)
            {
                // Return 404 code
                return NotFound();
            }

            return Ok(Arena);
        }

        // PUT: api/1.0/Arena/5
        [HttpPut("{id}")]
        public IActionResult PutArena([FromRoute] int id, [FromBody] Arena Arena)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Arena.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(Arena);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArenaExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return NoContent();
        }

        // POST: api/1.0/Arena
        [HttpPost]
        public IActionResult PostArena([FromBody] Arena Arena)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int? id = _repository.Add(Arena);

            return CreatedAtAction("GetArena", new { id = id }, Arena);
        }

        // DELETE: api/1.0/Arena/5
        [HttpDelete("{id}")]
        public IActionResult DeleteArena([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool Arena = ArenaExists(id);
            if (!Arena)
            {
                return NotFound();
            }

            _repository.Delete(id);

            return Ok();
        }

        private bool ArenaExists(int id)
        {
            return _repository.Find(id) != null;
        }
    }
}
