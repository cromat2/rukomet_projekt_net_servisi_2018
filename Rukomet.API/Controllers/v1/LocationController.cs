﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Entities;
using Rukomet.Repository;
using Rukomet.Repository.Interfaces;
using Rukomet.Repository.Repositories;

namespace Rukomet.API.Controllers.v1
{
    [Produces("application/json")]
    [ApiExplorerSettings(GroupName = "1.0")]
    [Route("api/{version:apiVersion}/Location")]
    [ApiVersion("1.0")]
    [Authorize]
    public class LocationController : Controller
    {
        private readonly ILocationRepository _repository;

        public LocationController(ILocationRepository repository)
        {
            _repository = repository;
        }

        // GET: api/1.0/Location
        [HttpGet]
        public IEnumerable<Location> GetLocations()
        {
            return _repository.GetAll();
        }

        // GET: api/1.0/Location/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Location = _repository.Find(id);

            if (Location == null)
            {
                return NotFound();
            }

            return Ok(Location);
        }

        // PUT: api/1.0/Location/5
        [HttpPut("{id}")]
        public IActionResult PutLocation([FromRoute] int id, [FromBody] Location Location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Location.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(Location);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocationExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return NoContent();
        }

        // POST: api/1.0/Location
        [HttpPost]
        public async Task<IActionResult> PostLocation([FromBody] Location Location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(Location);
            try
            {
                await _repository.GetContext().SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LocationExists(Location.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            
            return CreatedAtAction("GetLocation", new { id = Location.Id }, Location);
        }

        // DELETE: api/1.0/Location/5
        [HttpDelete("{id}")]
        public IActionResult DeleteLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool Location = LocationExists(id);
            if (!Location)
            {
                return NotFound();
            }

            _repository.Delete(id);

            return Ok();
        }

        private bool LocationExists(int id)
        {
            return _repository.Find(id) != null;
        }
    }
}
