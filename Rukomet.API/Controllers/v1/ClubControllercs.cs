﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Entities;
using Rukomet.Repository;
using Rukomet.Repository.Interfaces;
using Rukomet.Repository.Repositories;

namespace Rukomet.API.Controllers.v1
{
    [Produces("application/json")]
    [ApiExplorerSettings(GroupName = "1.0")]
    [Route("api/{version:apiVersion}/Club")]
    [ApiVersion("1.0")]
    [Authorize]
    public class ClubController : Controller
    {
        private readonly IClubRepository _repository;

        public ClubController(IClubRepository repository)
        {
            _repository = repository;
        }

        // GET: api/1.0/Club
        [HttpGet]
        public IEnumerable<Club> GetClubs()
        {
            return _repository.GetAll();
        }

        // GET: api/1.0/Club/5
        [HttpGet("{id}")]
        public IActionResult GetClub([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Club = _repository.Find(id);

            if (Club == null)
            {
                return NotFound();
            }

            return Ok(Club);
        }

        // PUT: api/1.0/Club/5
        [HttpPut("{id}")]
        public IActionResult PutClub([FromRoute] int id, [FromBody] Club Club)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Club.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(Club);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClubExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return NoContent();
        }

        // POST: api/1.0/Club
        [HttpPost]
        public IActionResult PostClub([FromBody] Club Club)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int? id = _repository.Add(Club);

            return CreatedAtAction("GetClub", new { id = id }, Club);
        }

        // DELETE: api/1.0/Club/5
        [HttpDelete("{id}")]
        public IActionResult DeleteClub([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool Club = ClubExists(id);
            if (!Club)
            {
                return NotFound();
            }

            _repository.Delete(id);

            return Ok();
        }

        private bool ClubExists(int id)
        {
            return _repository.Find(id) != null;
        }
    }
}
