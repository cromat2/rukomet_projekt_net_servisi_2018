﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Entities;
using Rukomet.Repository;
using Rukomet.Repository.Interfaces;
using Rukomet.Repository.Repositories;

namespace Rukomet.API.Controllers.v1
{
    [Produces("application/json")]
    [ApiExplorerSettings(GroupName = "1.0")]
    [Route("api/{version:apiVersion}/Player")]
    [ApiVersion("1.0")]
    [Authorize]
    public class PlayerController : Controller
    {
        private readonly IPlayerRepository _repository;

        public PlayerController(IPlayerRepository repository)
        {
            _repository = repository;
        }

        // GET: api/1.0/Player
        [HttpGet]
        public IEnumerable<Player> GetPLayers()
        {
            return _repository.GetAll();
        }

        // GET: api/1.0/Player/5
        [HttpGet("{id}")]
        public IActionResult GetPlayer([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Player = _repository.Find(id);

            if (Player == null)
            {
                return NotFound();
            }

            return Ok(Player);
        }

        // PUT: api/1.0/Player/5
        [HttpPut("{id:int:min(1)}")]
        public IActionResult PutPlayer([FromRoute] int id, [FromBody] Player Player)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Player.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(Player);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerExists(id))
                {
                    return NotFound();
                }
                throw;
            }

            return Ok();
        }

        // POST: api/1.0/Player
        [HttpPost]
        public IActionResult PostPlayer([FromBody] Player Player)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int? id = _repository.Add(Player);

            return CreatedAtAction("GetPlayer", new { id = id }, Player);
        }

        // DELETE: api/1.0/Player/5
        [HttpDelete("{id:int:min(1)}")]
        public IActionResult DeletePlayer([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool Player = PlayerExists(id);
            if (!Player)
            {
                return NotFound();
            }

            _repository.Delete(id);

            return Ok();
        }

        private bool PlayerExists(int id)
        {
            return _repository.Find(id) != null;
        }
    }
}
