﻿using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Rukomet.API.Helpers;
using Rukomet.API.Models;

namespace Rukomet.API.Controllers.v2
{
    [Route("api/{version:apiVersion}/Token")]
    [Produces("application/json")]
    [ApiVersion("2.0")]
    [ApiExplorerSettings(GroupName = "2.0")]
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost("RequestToken")]
        public IActionResult RequestToken([FromBody] TokenRequestModel tokenRequest)
        {
            if (tokenRequest == null)
            {
                return BadRequest();
            }

            var loginUser = true;

            if (loginUser == null)
            {
                return Unauthorized();
            }

            var token = JwsTokenCreator.CreateToken(
                tokenRequest.FirstName,
                _configuration["Auth:JwtSecurityKey"],
                _configuration["Auth:ValidIssuer"],
                _configuration["Auth:ValidAudience"]
            );

            string tokenStr = new JwtSecurityTokenHandler().WriteToken(token);

            return Ok(tokenStr);
        }
    }
}