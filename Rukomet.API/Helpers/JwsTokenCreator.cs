﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Rukomet.API.Helpers
{
    public static class JwsTokenCreator
    {
        public static JwtSecurityToken CreateToken(string firstName, string secret, string validIssuer, string validAudience)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, firstName)
            };

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            return new JwtSecurityToken(
                    validIssuer,
                    validAudience,
                    claims,
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: creds);
        }
    }
}