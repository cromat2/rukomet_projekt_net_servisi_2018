﻿namespace Rukomet.API.Models
{
    public class TokenRequestModel
    {
        public string FirstName { get; set; }

        public string SecretHash { get; set; }
    }
}