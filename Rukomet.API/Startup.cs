﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Rukomet.API.Helpers;
using Rukomet.DAL.Context;
using Rukomet.Repository.Interfaces;
using Rukomet.Repository.Repositories;
using Swashbuckle.AspNetCore.Swagger;

namespace Rukomet.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IPlayerRepository, PlayerRepository>();
            services.AddSingleton<ILocationRepository, LocationRepository>();
            
            services.AddMvc(options => { options.RequireHttpsPermanent = true; })
                .AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<RukometContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
                        
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Auth:ValidIssuer"],
                        ValidAudience = Configuration["Auth:ValidAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Auth:JwtSecurityKey"]))
                    };
                })
                .AddGoogle(googleOptions =>
                {
                    googleOptions.ClientId = Configuration["Auth:Google:client_id"];
                    googleOptions.ClientSecret = Configuration["Auth:Google:client_secret"];
                    googleOptions.AuthorizationEndpoint= Configuration["Auth:Google:auth_uri"];
                });;
            

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("1.0", new Info { Title = "Rukomet API V1", Version = "1.0" });
                c.SwaggerDoc("2.0", new Info { Title = "Rukomet API V2", Version = "2.0" });
            });
            
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/1.0/swagger.json", "Rukomet API V1");
                c.SwaggerEndpoint("/swagger/2.0/swagger.json", "Rukomet API V2");
            });
        }
    }
}