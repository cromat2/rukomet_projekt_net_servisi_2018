﻿using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Context;
using Rukomet.DAL.Entities;
using Rukomet.Repository.Interfaces;

namespace Rukomet.Repository.Repositories
{
    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(RukometContext context) : base(context)
        {
        }

        public DbContext GetContext()
        {
            return this.Context;
        }
    }
}