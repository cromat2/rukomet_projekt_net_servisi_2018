﻿using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Context;
using Rukomet.DAL.Entities;
using Rukomet.Repository.Interfaces;

namespace Rukomet.Repository.Repositories
{
    public class LocationRepository : BaseRepository<Location>, ILocationRepository
    {
        public LocationRepository(RukometContext context) : base(context)
        {
        }

        public DbContext GetContext()
        {
            return this.Context;
        }
    }
}