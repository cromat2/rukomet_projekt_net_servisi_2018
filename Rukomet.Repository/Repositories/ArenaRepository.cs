﻿using Rukomet.DAL.Context;
using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Repositories
{
    public class ArenaRepository : BaseRepository<Arena>
    {
        public ArenaRepository(RukometContext context) : base(context)
        {
        }
    }
}