﻿using Rukomet.DAL.Context;
using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Repositories
{
    public class ClubRepository : BaseRepository<Club>
    {
        public ClubRepository(RukometContext context) : base(context)
        {
        }
    }
}