﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Entities;
using Rukomet.DAL.Context;
using Rukomet.Repository.Interfaces;

namespace Rukomet.Repository.Repositories
{
    public abstract class BaseRepository<TEntity> where TEntity : EntityBase
    {
        protected RukometContext Context;

        protected BaseRepository(RukometContext context)
        {
            this.Context = context;
        }

        public IEnumerable<TEntity> GetAll()
        {
            var All = this.Context.Set<TEntity>();
            if (All == null)
                return new List<TEntity>();
            return All;
        }

        public virtual TEntity Find(int id)
        {
            return this.Context
                .Set<TEntity>()
                .FirstOrDefault(p => p.Id == id);
        }

        public int? Add(TEntity model)
        {
            this.Context.Set<TEntity>().Add(model);
            this.Save();

            return model.Id;
        }

        public void Update(TEntity model)
        {
            this.Context.Entry(model).State = EntityState.Modified;
            this.Save();
        }

        public virtual void Delete(int id)
        {
            var entity = this.Context.Set<TEntity>().Find(id);
            this.Context.Entry(entity).State = EntityState.Deleted;
            this.Save();
        }

        private void Save()
        {
            this.Context.SaveChanges();
        }
    }
}