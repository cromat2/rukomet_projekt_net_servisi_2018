﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : EntityBase
    {
        DbContext GetContext();
        
        IEnumerable<TEntity> GetAll();

        TEntity Find(int id);

        int? Add(TEntity model);

        void Update(TEntity model);

        void Delete(int id);
    }
}