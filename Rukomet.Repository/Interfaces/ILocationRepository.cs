﻿using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Interfaces
{
    public interface ILocationRepository : IBaseRepository<Location>
    {
        
    }
}