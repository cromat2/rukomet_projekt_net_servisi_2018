﻿using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Interfaces
{
    public interface IPlayerRepository : IBaseRepository<Player>
    {
        
    }
}