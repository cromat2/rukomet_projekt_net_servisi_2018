﻿using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Interfaces
{
    public interface IArenaRepository : IBaseRepository<Arena>
    {
        
    }
}