﻿using Rukomet.DAL.Entities;

namespace Rukomet.Repository.Interfaces
{
    public interface IClubRepository : IBaseRepository<Club>
    {
        
    }
}