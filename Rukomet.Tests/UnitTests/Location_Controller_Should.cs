﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rukomet.API.Controllers.v1;
using Rukomet.DAL.Context;
using Rukomet.DAL.Entities;
using Rukomet.Repository.Repositories;
using Xunit;

namespace Rukomet.Tests.UnitTests
{
    public class Location_Controller_Should
    {

        DbContextOptions<RukometContext> _dbContextOptions;
        private LocationRepository _locationRepository;

        public Location_Controller_Should()
        {
            _dbContextOptions = new DbContextOptionsBuilder<RukometContext>()
                .UseInMemoryDatabase(databaseName: "Test_database")
                .Options;
        }
        
        
        [Fact]
        public async void PostLocation()
        {
            using (var context = new RukometContext(_dbContextOptions))
            {
                _locationRepository = new LocationRepository(context);
                var locationApi = new LocationController(_locationRepository);
                
                for (int i = 1; i <= 5; i++)
                {
                    Location location = new Location();
                    location.Country = "Country" + i.ToString();
                    location.City = "City" + i.ToString();
                    
                    var result = await locationApi.PostLocation(location);
                    var badRequest = result as BadRequestObjectResult;

                    Assert.Null(badRequest);
                }
            }
        }

        [Fact]
        public async void GetLocation()
        {
            // Posting locations first
            using (var context = new RukometContext(_dbContextOptions))
            {
                _locationRepository = new LocationRepository(context);
                var locationApi = new LocationController(_locationRepository);
                
                for (int i = 1; i <= 5; i++)
                {
                    Location location = new Location();
                    location.Country = "Country" + i.ToString();
                    location.City = "City" + i.ToString();
                    
                    await locationApi.PostLocation(location);
                }
            }
            
            using (var context = new RukometContext(_dbContextOptions))
            {
                _locationRepository = new LocationRepository(context);
                var locationApi = new LocationController(_locationRepository);
                var result = await locationApi.GetLocation(1);
                var okResult = result as OkObjectResult;

                Assert.NotNull(okResult);
                Assert.Equal(200, okResult.StatusCode);

                Location location = okResult.Value as Location;
                Assert.NotNull(location);
                Assert.Equal("Country1", location.Country);
            }
        }
    }
}