﻿using Rukomet.DAL.Entities;
using Xunit;

namespace Rukomet.Tests.UnitTests
{
    public class Player_Should
    {
        private Player player;

        public Player_Should()
        {
            player = new Player();
            player.FirstName = "Juro";
            player.LastName = "Juric";
            player.Number = 22;
            player.Position = "Pivot";
        }

        [Fact]
        public void FullNameHasValue()
        {
            Assert.Equal(player.FirstName + " " + player.LastName, player.getFullName());
        }
    }
}