﻿namespace Rukomet.DAL.Entities
{
    public class Player : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Number { get; set; }
        public Club Club { get; set; }
        public string Position { get; set; }
       
        public string getFullName()
        {
            return this.FirstName + " " + this.LastName;
        }
    }
}