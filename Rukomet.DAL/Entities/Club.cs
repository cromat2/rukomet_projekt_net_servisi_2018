﻿using System.Collections.Generic;

namespace Rukomet.DAL.Entities
{
    public class Club : EntityBase
    {
        public ICollection<Player> Players { get; set; }
        public string JerseyColor { get; set; }
        public int YearEst { get; set; }
        public Arena Arena { get; set; }
    }
}