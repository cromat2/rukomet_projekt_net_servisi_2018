﻿using System.Collections.Generic;

namespace Rukomet.DAL.Entities
{
    public class Arena : EntityBase
    {
        public int Capacity { get; set; }
        public Location Location { get; set; }
        public int BuildYear { get; set; }
        public ICollection<Club> Clubs { get; set; }
    }
}