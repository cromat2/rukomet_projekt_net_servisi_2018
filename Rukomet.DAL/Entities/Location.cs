﻿using System.Collections.Generic;

namespace Rukomet.DAL.Entities
{
    public class Location : EntityBase
    {
        public string Country { get; set; }
        public string City { get; set; }
        public ICollection<Arena> Arenas { get; set; }

        public Location()
        {
            this.Arenas = new List<Arena>();
        }
    }
}