﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Rukomet.DAL.Entities;

namespace Rukomet.DAL.Context
{
    public class RukometContext : DbContext
    {
        public RukometContext(DbContextOptions<RukometContext> options) : base(options) { }
        
        public DbSet<Arena> Arenas { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Player> Players { get; set; }
    }
    
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<RukometContext>
    {
        public RukometContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<RukometContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
            return new RukometContext(builder.Options);
        }
    }
}